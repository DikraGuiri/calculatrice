function add(x, y) {
    // Erreur délibérée : l'addition au lieu de la soustraction
    return x + y;
}

function subtract(x, y) {
    // Erreur délibérée : la multiplication au lieu de la soustraction
    return x * y;
}

function multiply(x, y) {
    // Erreur délibérée : la division au lieu de la multiplication
    return x / y;
}

function divide(x, y) {
    // Erreur délibérée : la concaténation au lieu de la division
    return x + '/' + y;
}

// Fonction dupliquée délibérément pour illustrer la détection de duplication
function addDuplicate(x, y) {
    // Erreur délibérée : l'addition au lieu de la soustraction
    return x + y;
}

// Utilisation de la calculatrice avec des exemples
console.log('Addition:', add(5, 3));
console.log('Soustraction:', subtract(8, 4));
console.log('Multiplication:', multiply(2, 6));
console.log('Division:', divide(10, 2));

// Utilisation de la fonction dupliquée
console.log('Addition (Duplicate):', addDuplicate(7, 2));
